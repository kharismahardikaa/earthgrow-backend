<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->bigIncrements('id_campaign');
            $table->bigInteger('id_user')->unsigned();
            $table->foreign('id_user')->references('id_user')->on('users');
            $table->bigInteger('id_category_campaign')->unsigned();
            $table->foreign('id_category_campaign')->references('id_category_campaign')->on('category_campaigns');
            $table->string('campaign_name');
            $table->string('campaign_description');
            $table->bigInteger('money_amount');
            $table->date('deadline_campaign');
            $table->date('campaign_start');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
