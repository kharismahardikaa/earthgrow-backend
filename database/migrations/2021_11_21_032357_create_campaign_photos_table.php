<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_photos', function (Blueprint $table) {
            $table->bigIncrements('id_campaign_photo');
            $table->bigInteger('id_campaign')->unsigned();
            $table->foreign('id_campaign')->references('id_campaign')->on('campaigns');
            $table->string('name_campaign_photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_photos');
    }
}
