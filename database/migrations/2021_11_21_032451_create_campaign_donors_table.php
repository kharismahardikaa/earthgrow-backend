<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignDonorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_donors', function (Blueprint $table) {
            $table->bigIncrements('id_campaign_donor');
            $table->bigInteger('id_campaign')->unsigned();
            $table->foreign('id_campaign')->references('id_campaign')->on('campaigns');
            $table->string('donor_name');
            $table->string('donor_telephone');
            $table->string('donor_address');
            $table->bigInteger('donation_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_donors');
    }
}
