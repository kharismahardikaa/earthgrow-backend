<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptCampaignDonorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_campaign_donors', function (Blueprint $table) {
            $table->bigIncrements('id_receipt');
            $table->bigInteger('id_campaign_donor')->unsigned();
            $table->foreign('id_campaign_donor')->references('id_campaign_donor')->on('campaign_donors');
            $table->string('name_receipt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_campaign_donors');
    }
}
