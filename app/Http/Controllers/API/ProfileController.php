<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id_user)
    {
        $user = User::where('id_user', $id_user)->select('full_name', 'email', 'no_telephone', 'identity_number', 'address', 'user_photo')->first();
        return response()->json([
            'user' => $user,
            'message' => 'success get data',
            'status' => 200
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id_user)
    {
        if($request->json()){
            $data = $request->validate([
                'full_name' => 'required',
                'email' => 'required',
                'no_telephone' => 'required',
                'identity_number' => 'required',
                'address' => 'required',
//                'user_photo' => 'required'
            ]);

            $userProfile = User::where('id_user', '=', $id_user)->first();

            if($data && $userProfile != null){
                $updateUser = $userProfile->update([
                    'full_name' => $data['full_name'],
                    'email' => $data['email'],
                    'no_telephone' => $data['no_telephone'],
                    'identity_number' => $data['identity_number'],
                    'address' => $data['address'],
//                    'user_photo' => $data['user_photo']
                ]);

                $user = User::where('id_user', '=', $id_user)->first();

                return response()->json([
                    'status' => 200,
                    'updateUser' => $user,
                    'message' => 'Data profil berhasil diubah'
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
