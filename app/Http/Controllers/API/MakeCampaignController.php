<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\CampaignDonor;
use App\Models\CampaignPhoto;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MakeCampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $campaign = Campaign::all();
        return response()->json([
           'campaign' => $campaign
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if($request->json()){
            $data = $request->validate([
               'id_user' => 'required',
               'id_category_campaign' => 'required',
               'campaign_name' => 'required',
               'campaign_description' => 'required',
               'money_amount' => 'required',
               'deadline_campaign' => 'required',
               'campaign_start' => 'required'
            ]);

            if($data){
                $campaign = Campaign::create([
                    'id_user' => $data['id_user'],
                    'id_category_campaign' => $data['id_category_campaign'],
                    'campaign_name' => $data['campaign_name'],
                    'campaign_description' => $data['campaign_description'],
                    'money_amount' => $data['money_amount'],
                    'deadline_campaign' => Carbon::parse($data['deadline_campaign'])->format('Y-m-d'),
                    'campaign_start' => Carbon::parse($data['campaign_start'])->format('Y-m-d')
                ]);

                return response()->json([
                   'status' => 200,
                   'message' => 'Campaign has been created',
                   'campaign' => $campaign
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id_campaign)
    {
        $totalCollected = CampaignDonor::where('id_campaign', '=', $id_campaign)->sum('donation_amount');
        $campaign = Campaign::where('id_campaign', '=', $id_campaign)->first();
        return response()->json([
            'status' => 200,
            'campaign' => $campaign,
            'totalCollected' => $totalCollected
        ]);
    }


    public function showAllCampaign($id_user){
        $campaigns = Campaign::where('id_user', '=', $id_user)->get();
        return response()->json([
            'campaigns' => $campaigns,
            'status' => 200
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id_campaign)
    {
        if($request->json()){
            $data = $request->validate([
                'id_user' => 'required',
                'id_category_campaign' => 'required',
                'campaign_name' => 'required',
                'campaign_description' => 'required',
                'money_amount' => 'required',
                'deadline_campaign' => 'required',
                'campaign_start' => 'required'
            ]);

            $campaign = Campaign::where('id_campaign', '=', $id_campaign)->first();

            if($data){
                $dataCampaign = $campaign->update([
                    'id_user' => $data['id_user'],
                    'id_category_campaign' => $data['id_category_campaign'],
                    'campaign_name' => $data['campaign_name'],
                    'campaign_description' => $data['campaign_description'],
                    'money_amount' => $data['money_amount'],
                    'deadline_campaign' => Carbon::parse($data['deadline_campaign'])->format('Y-m-d'),
                    'campaign_start' => Carbon::parse($data['campaign_start'])->format('Y-m-d')
                ]);

                $campaignUser = Campaign::where('id_campaign', '=', $id_campaign)->first();

                return response()->json([
                    'status' => 200,
                    'message' => 'Campaign has been updated',
                    'campaign' => $campaignUser
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id_campaign)
    {
        $campaignPhoto = CampaignPhoto::where('id_campaign', '=', $id_campaign)->first();
        $campaignDonor = CampaignDonor::where('id_campaign', '=', $id_campaign)->first();

        if($campaignPhoto == null && $campaignDonor ==null){
            $campaign = Campaign::findOrFail($id_campaign);
            $campaign->delete();

            return response()->json([
                'status' => 200,
                'message' => 'campaign has been deleted'
            ]);
        } else {
            return response()->json([
               'status' => 401,
               'message' => 'Campaign has running'
            ]);
        }
    }
}
