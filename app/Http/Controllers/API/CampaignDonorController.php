<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CampaignDonor;
use App\Models\ReceiptCampaignDonor;
use Illuminate\Http\Request;

class CampaignDonorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if($request->json()){
            $data = $request->validate([
               'id_campaign' => 'required',
               'donor_name' => 'required',
               'donor_telephone' => 'required',
               'donor_address' => 'required',
               'donation_amount' => 'required',
               'name_receipt' => 'required'
            ]);

            if($data){
                $campaignDonor = CampaignDonor::create([
                    'id_campaign' => $data['id_campaign'],
                    'donor_name' => $data['donor_name'],
                    'donor_telephone' => $data['donor_telephone'],
                    'donor_address' => $data['donor_address'],
                    'donation_amount' => $data['donation_amount']
                ]);

                $data['name_receipt'] = $request->file('name_receipt')->store('assets/gallery/receipt', 'public');

                ReceiptCampaignDonor::create([
                    'id_campaign_donor' => $campaignDonor->id_campaign_donor,
                    'name_receipt' => $data['name_receipt']
                ]);

                return response()->json([
                    'status' => 200,
                    'message' => 'Your donation has been sent',
                    'campaignDonor' => $campaignDonor
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
