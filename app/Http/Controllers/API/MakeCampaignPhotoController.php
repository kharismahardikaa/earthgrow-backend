<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\CampaignPhoto;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class MakeCampaignPhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if($request->json()){
            $data = $request->validate([
               'id_campaign'  => 'required',
               'name_campaign_photo' => 'required'
            ]);

            if($data){
                $data['name_campaign_photo'] = $request->file('name_campaign_photo')->store('assets/gallery/campaign', 'public');

                $campaignPhoto = CampaignPhoto::create([
                   'id_campaign' => $data['id_campaign'],
                   'name_campaign_photo' => $data['name_campaign_photo']
                ]);

                return response()->json([
                   'status' => 200,
                   'message' => 'success added campaign photo',
                   'campaignPhoto' => $campaignPhoto
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $campaignPhoto = CampaignPhoto::findOrFail($request->id_campaign_photo);
        File::delete(public_path('storage/'.$campaignPhoto->name_campaign_photo));
        $campaignPhoto->delete();

        return response()->json([
           'status' => 200,
           'message' => 'Campaign photo has been deleted'
        ]);
    }
}
