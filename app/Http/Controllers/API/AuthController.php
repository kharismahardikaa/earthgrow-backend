<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request){
        $data = $request->validate([
            'full_name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'no_telephone' => 'required',
            'identity_number' => 'required',
            'address' => 'required'
        ]);

        if($data){
            if($request->user_photo){
                $data['user_photo'] = $request->file('user_photo')->store('assets/gallery/profile', 'public');

                $user = User::create([
                    'full_name' => $data['full_name'],
                    'email' => $data['email'],
                    'password' => Hash::make($data['password']),
                    'no_telephone' => $data['no_telephone'],
                    'identity_number' => $data['identity_number'],
                    'address' => $data['address'],
                    'user_photo' => $data['user_photo']
                ]);
            } else {
                $user = User::create([
                    'full_name' => $data['full_name'],
                    'email' => $data['email'],
                    'password' => Hash::make($data['password']),
                    'no_telephone' => $data['no_telephone'],
                    'identity_number' => $data['identity_number'],
                    'address' => $data['address']
                ]);
            }

            return response()->json([
                'user' => $user,
                'message' => 'Successfully registered',
                'status' => 200
            ]);
        }
    }

    public function login(Request $request){
        $user = User::where('email', $request->email)->first();
        if(!$user || !Hash::check($request->password, $user->password)){
            return response()->json([
               'message' => 'Wrong email or password',
               'status' => 404
            ]);
        }

        $token = $user->createToken('ApiToken')->plainTextToken;

        $dataUser = User::where('email', $request->email)->select('full_name', 'email', 'no_telephone','identity_number', 'address', 'user_photo')->first();

        return response()->json([
           'user' => $dataUser,
           'message' => 'Success loggin',
           'token' => $token,
           'status' => 200
        ]);

    }

    public function logout(Request $request){
        $user = $request->user();
        $user->currentAccessToken()->delete();

        return response()->json([
           'message' => 'Success logout',
           'status' => 200
        ]);
    }
}
