<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryCampaign extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_category_campaign';
    protected $fillable = [
        'name_category_campaign'
    ];

    public function campaign(){
        return $this->hasMany(Campaign::class);
    }
}
