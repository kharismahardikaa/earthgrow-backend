<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_campaign';
    protected $fillable = [
        'id_user', 'id_category_campaign', 'campaign_name', 'campaign_description', 'money_amount', 'deadline_campaign', 'campaign_start'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'id_user', 'id_user');
    }

    public function categoryCampaign(){
        return $this->belongsTo(CategoryCampaign::class, 'id_category_campaign', 'id_category_campaign');
    }

    public function campaignPhoto(){
        return $this->hasMany(CampaignPhoto::class);
    }

    public function campaignDonor(){
        return $this->hasMany(CampaignPhoto::class);
    }
}
