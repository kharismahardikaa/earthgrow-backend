<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReceiptCampaignDonor extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_receipt';
    protected $fillable = [
        'name_receipt', 'id_campaign_donor'
    ];

    public function campaignDonor(){
        return $this->belongsTo(CampaignDonor::class, 'id_campaign_donor', 'id_campaign_donor');
    }
}
