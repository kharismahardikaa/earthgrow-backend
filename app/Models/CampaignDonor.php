<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampaignDonor extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_campaign_donor';
    protected $fillable = [
        'id_campaign', 'donor_name', 'donor_telephone', 'donor_address', 'donation_amount'
    ];

    public function receiptCampaignDonor(){
        return $this->hasOne(ReceiptCampaignDonor::class);
    }

    public function campaign(){
        return $this->belongsTo(Campaign::class, 'id_campaign', 'id_campaign');
    }
}
