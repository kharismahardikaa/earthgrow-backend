<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampaignPhoto extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_campaign_photo';
    protected $fillable = [
        'name_campaign_photo', 'id_campaign'
    ];

    public function campaign(){
        return $this->belongsTo(Campaign::class, 'id_campaign', 'id_campaign');
    }
}
