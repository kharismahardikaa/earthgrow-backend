<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:sanctum'], function (){

    //campaign route
    Route::post('/add-campaign', [\App\Http\Controllers\API\MakeCampaignController::class, 'store']);
    Route::get('/show-all-campaign/{id_user}', [\App\Http\Controllers\API\MakeCampaignController::class, 'showAllCampaign']);
    Route::put('/update-campaign/{id_campaign}', [\App\Http\Controllers\API\MakeCampaignController::class, 'update']);
    Route::delete('/delete-campaign/{id_campaign}', [\App\Http\Controllers\API\MakeCampaignController::class, 'destroy']);

    //campaign photo route
    Route::post('/add-campaign-photo', [\App\Http\Controllers\API\MakeCampaignPhotoController::class, 'store']);
    Route::delete('/delete-campaign-photo', [\App\Http\Controllers\API\MakeCampaignPhotoController::class, 'destroy']);

    //profile route
    Route::get('/show-profile/{id_user}', [\App\Http\Controllers\API\ProfileController::class, 'show']);
    Route::put('/update-profile/{id_user}', [\App\Http\Controllers\API\ProfileController::class, 'update']);

    //auth route
    Route::post('/logout', [\App\Http\Controllers\API\AuthController::class, 'logout']);
});

Route::post('/login', [\App\Http\Controllers\API\AuthController::class, 'login']);
Route::post('/register', [\App\Http\Controllers\API\AuthController::class, 'register']);

//campaign donor route
Route::post('/add-campaign-donor', [\App\Http\Controllers\API\CampaignDonorController::class, 'store']);
Route::get('/show-campaign/{id_campaign}', [\App\Http\Controllers\API\MakeCampaignController::class, 'show']);
Route::get('/show-campaign-all', [\App\Http\Controllers\API\MakeCampaignController::class, 'index']);
